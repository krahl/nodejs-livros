// fs é um pacote JavaScript usado para mexer com arquivos, ler, escrever, excluir e etc..
const fs = require('fs');

module.exports = {
    carregarBancoDeDados(){
        //Aqui vamos carregar o que tem no arquivo banco_dados.json
        return JSON.parse(fs.readFileSync("banco_dados.json","utf8"))
    },

    atualizarBancoDeDados(dadosDoBanco){
        //Aqui vamos salvar os dados no arquivo
        return fs.writeFileSync('banco_dados.json', JSON.stringify(dadosDoBanco));
    },

    novoLivro(titulo, autor, quantidadeDePaginas) {//Vamos adicionar um novo banco
        //Vamos carregar o que tem no banco
        let bancoDeDados = this.carregarBancoDeDados()

        //Vamos gerar o código desse livro
        const codigoDoLivro = bancoDeDados.contador_livros+1
        console.log('Numero do próximo livro é:', codigoDoLivro)

        //Vamos montar o livro
        let livroQueVamosAdicionar = {
            title: titulo,
            author: autor,
            pages: quantidadeDePaginas,
            id: codigoDoLivro
        }

        //Agora vamos adicionar o livro no banco
        bancoDeDados.livros.push(livroQueVamosAdicionar)
        //Também precisamos atualizar qual é o ultimo contador de livros
        bancoDeDados.contador_livros = codigoDoLivro


        //Agora salvamos o banco de dados
        this.atualizarBancoDeDados(bancoDeDados)
    },
    
    atualizarLivro(numeroDoLivro, titulo, autor, quantidadeDePaginas) {
        let bancoDeDados = this.carregarBancoDeDados()

        for (let index = 0; index < bancoDeDados.livros.length; index++) {
            if(bancoDeDados.livros[index].id == numeroDoLivro){
                bancoDeDados.livros[index] = {
                    id: numeroDoLivro,
                    author: autor,
                    title: titulo,
                    pages: quantidadeDePaginas 
                }
                //Depois que atualizar o livro para o FOR
                break
            }
        }

        //Agora atualizamos no banco
        this.atualizarBancoDeDados(bancoDeDados)
    },
    
    buscarTodosOsLivros() {
        const bancoDeDados = this.carregarBancoDeDados()
        return bancoDeDados.livros
    },

    buscarLivro(numeroDoLivro) {
        const bancoDeDados = this.carregarBancoDeDados()
        const livros = bancoDeDados.livros

        for (let index = 0; index < livros.length; index++) {
            if(livros[index].id == numeroDoLivro){
                return livros[index]
            }
        }

        return 'livro não encontado...'
    },

    excluirLivro(numeroDoLivro){
        let bancoDeDados = this.carregarBancoDeDados()

        for (let index = 0; index < bancoDeDados.livros.length; index++) {
            if(bancoDeDados.livros[index].id == numeroDoLivro){
                // Aqui vamos remover 1 livro, quando achar a posição que ele esta
                bancoDeDados.livros.splice(index, 1); 
            }
        }

        this.atualizarBancoDeDados(bancoDeDados)
    }
}